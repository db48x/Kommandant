
#
# Makefile for kommandant
#

build:
	go build demos/demo-readline.go
test:
	go test ./...
govet:
	go vet ./...
gofmt goformat:
	go fmt ./...
#golint:
